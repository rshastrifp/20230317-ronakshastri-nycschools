//
//  SchoolDetailsApi.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import Foundation

struct SchoolDetailsApiQuery: Decodable {
    var dbn: String
}

class SchoolDetailsApi: API {
    override var uri: String {
        if let path = EnvironmentConfig.NycSchoolApiList[EnvironmentConfig.pathKey.schoolDetails] {
            return String(format: "%@%@", SchoolDetailsApi.baseUrl, path)
        } else {
            fatalError("path needs to be defined in Apis.plist file.")
        }
    }
    
    init(apiQueries: SchoolDetailsApiQuery) {
        super.init()
        //We can pass this queries using a struct from the viewmodel as well.
        // Currently just using static values.
        let queries = [
            "dbn" : String(apiQueries.dbn)
        ]
        for (key, value) in queries {
            self.queryParams[key] = value
        }
    }
    
    
    func fetchData(completion: @escaping (Result<[SchoolDetailsRespnse], Error>) -> Void) {
        ApiClient().execute(api: self, params: self.queryParams) { (result: Result< [SchoolDetailsRespnse] , Error>) in
            switch result {
            case .success(let list):
                completion(.success(list))
                break
            case .failure(let error):
                completion(.failure(error))
                break
            }
        }
    }
}

