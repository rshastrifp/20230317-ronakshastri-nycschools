//
//  SchoolApi.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import Foundation

struct SchoolApiQuery {
    var limit: Int
}

class SchoolApi: API {
    override var uri: String {
        if let path = EnvironmentConfig.NycSchoolApiList[EnvironmentConfig.pathKey.schoolList] {
            return String(format: "%@%@", SchoolApi.baseUrl, path)
        } else {
            fatalError("path needs to be defined in Apis.plist file.")
        }
    }
    
    init(apiQueries: SchoolApiQuery) {
        super.init()
        //We can pass this queries using a struct from the viewmodel as well.
        // Currently just using static values.
        let queries = [
            "$limit" : String(apiQueries.limit)
        ]
        for (key, value) in queries {
            self.queryParams[key] = value
        }
    }
    
    
    func fetchData(completion: @escaping (Result<[SchoolResponseModel], Error>) -> Void) {
        ApiClient().execute(api: self, params: self.queryParams) { (result: Result< [SchoolResponseModel] , Error>) in
            switch result {
            case .success(let list):
                completion(.success(list))
                break
            case .failure(let error):
                completion(.failure(error))
                break
                
            }
        }
    }
}

