//
//  SchoolResponseModel.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import Foundation

struct SchoolResponseModel: Decodable {
    let dbn: String
    let school_name: String
    let city: String
}
