//
//  ApiClient.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import Foundation

class ApiClient {
    public func execute<T: Decodable>(api: API, params: [String: String], completion: @escaping (Result<T, Error>) -> Void) {
        
        guard var urlComp = URLComponents(string: api.uri) else { return }
        
        var items = [URLQueryItem]()
        for (key,value) in params {
            items.append(URLQueryItem(name: key, value: value))
        }
        items = items.filter{!$0.name.isEmpty}
        if !items.isEmpty {
            urlComp.queryItems = items
        }
        var urlRequest = URLRequest(url: urlComp.url!)
        urlRequest.httpMethod = "GET"
        
        //Alamofire can also be used here for many advanced functions of web API.
        //I have used Alamofire in past. 
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            
            guard let data = data, let httpResponse = response as? HTTPURLResponse, error == nil else {
                print("Not a valid Response.")
                return
            }
            
            guard 200 ..< 300 ~= httpResponse.statusCode else {
                print("Status code was \(httpResponse.statusCode), but expected 2xx")
                completion(Result.failure(error ?? NSError(domain: "Error", code: httpResponse.statusCode)))
                return
            }
            
            do {
                let jsonDecoded = try JSONDecoder().decode(T.self, from: data)
                completion(.success(jsonDecoded))
            } catch {
                completion(.failure(error))
            }
        }).resume()
    }
    
    
}
