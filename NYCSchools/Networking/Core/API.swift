//
//  API.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import Foundation
protocol ApiProtocol {
    var uri: String { get }
    var queryParams: [String: String] {get set}
    var pathParams: [String: String] {get set}
}

class API {
    static let baseUrl = EnvironmentConfig.baseUrl
    
    var pathParams: [String: String] = [String: String]()
    
    var uri: String {
        fatalError("API uri Has to be overridden.")
    }
    
    //If in case in fure we want to have query Params. 
    var queryParams: [String: String] = [String: String]()
}
