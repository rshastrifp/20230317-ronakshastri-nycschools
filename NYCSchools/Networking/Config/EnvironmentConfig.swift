//
//  EnvironmentConfig.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import Foundation

import Foundation

struct EnvironmentConfig {
    
    //We can config this base url depeding on environment. 
    static var baseUrl = "https://data.cityofnewyork.us"
    
    struct pathKey {
        static var schoolList: String {return "schools"}
        static var schoolDetails: String {return "schoolDetails"}
    }
    
    static var NycSchoolApiList:[String: String] {
        return NSDictionary(contentsOfFile: ApiFilePath) as? [String:String] ?? [:]
    }
    
    private static var ApiFilePath: String {
        return Bundle.main.path(forResource: "Apis", ofType: "plist")!
    }
}
