//
//  SchoolListTableViewCell.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCity: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(schoolViewModel: SchoolViewModel) {
        self.labelName.text = schoolViewModel.schoolName
        self.labelCity.text = schoolViewModel.schoolCity
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
