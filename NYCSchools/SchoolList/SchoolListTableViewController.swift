//
//  SchoolListTableViewController.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import UIKit

class SchoolListTableViewController: UITableViewController {
    
    var viewModel = SchoolListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false
        self.fetchSchoolList()
    }
    
    func fetchSchoolList() {
        viewModel.fetchSchoolList { [weak self] result in
            guard let self  = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.tableView.reloadData()
                    break
                case .failure( let error):
                    self.showAlertError(error: error)
                    break
                }
            }
        }
    }
    
    func showAlertError(error: Error) {
        let alertViewController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)

        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertViewController.addAction(okAction)

        present(alertViewController, animated: true, completion: nil)
    }
}

extension SchoolListTableViewController {
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.listSchools.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "schoolNameCell", for: indexPath) as? SchoolListTableViewCell {
            let viewModel = viewModel.listSchools[indexPath.row]
            cell.configureCell(schoolViewModel: viewModel)
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Given more time I would move this logic for fetching the details in to next viewcontroller.
        viewModel.fetchDetails(dbn: viewModel.listSchools[indexPath.row].dbn) { result in
            DispatchQueue.main.async {
                switch result {
                case .success():
                    self.performSegue(withIdentifier: "showDetails", sender: self)
                case .failure(let error):
                    self.showAlertError(error: error)
                }
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
                guard let detailsViewController = segue.destination as? SchoolDetailsViewController else {
                    return
                }
                
            //Given more time I would create a view model as well which will
            //wrap this detials in to a struct plus I fetch these details in
            //next viewcontroller (details View Controller) .
                if let selectedSchoolDetails = viewModel.selectedSchoolSatDetails {
                    detailsViewController.schoolTitle = selectedSchoolDetails.schoolName
                    detailsViewController.mathScore = selectedSchoolDetails.satMath
                    detailsViewController.readingScore = selectedSchoolDetails.satReading
                    detailsViewController.writingScore = selectedSchoolDetails.satWriting
                }
            }
    }
}
