//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

import Foundation

struct SchoolViewModel {
    let dbn: String
    let schoolName: String
    let schoolCity: String
}

struct SchoolSatDetails {
    let schoolName: String
    let satMath: String
    let satReading: String
    let satWriting: String
}

class SchoolListViewModel {
    var listSchools = [SchoolViewModel]()
    var selectedSchoolSatDetails: SchoolSatDetails?
    
    func fetchSchoolList(completion: @escaping (Result<Void, Error>) -> Void) {
        
        //I have limit the fetch to 50 schools only
        //but this can be adjusted to fetch more or later if needed
        //we can introduce pagenation or lazy loading as well. 
        let schoolApiQueries = SchoolApiQuery(limit: 50)
        SchoolApi(apiQueries: schoolApiQueries).fetchData { [weak self] result in
            guard let self  = self else { return }
            switch result {
            case .success(let list):
                self.listSchools = list.map { schoolResponseModel in
                    SchoolViewModel(dbn: schoolResponseModel.dbn,
                                    schoolName: schoolResponseModel.school_name,
                                    schoolCity: schoolResponseModel.city)
                }
                completion(.success(()))
                break
            case .failure( let error):
                completion(.failure(error))
                break
            }
        }
    }
    
    //I prefer moving this fetch to new details VC.
    func fetchDetails(dbn: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let detailsAPiQuery = SchoolDetailsApiQuery(dbn: dbn)
        SchoolDetailsApi(apiQueries: detailsAPiQuery).fetchData { result in
            switch result {
            case .success(let list):
                if list.count > 0,
                   let firstSchool = list.first {
                    self.selectedSchoolSatDetails = SchoolSatDetails(
                        schoolName: firstSchool.schoolName,
                        satMath: firstSchool.satMathAvgScore,
                        satReading: firstSchool.satCriticalReadingAvgScore,
                        satWriting: firstSchool.satWritingAvgScore)
                    completion(.success(()))
                } else {
                    //I added this error cause I observe for some schools there are no details coming some times.
                    completion(.failure(NSError(domain: "No Data Found for this school", code: 404)))
                }
            case .failure(let error):
                completion(.failure(error))
                break
            }
        }
    }

}
