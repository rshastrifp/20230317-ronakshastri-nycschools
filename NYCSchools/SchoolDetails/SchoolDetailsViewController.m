//
//  SchoolDetailsViewController.m
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

#import "SchoolDetailsViewController.h"

@interface SchoolDetailsViewController ()

@end

@implementation SchoolDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _labelSchoolTitle.text = _schoolTitle;
    _labelMathScore.text = _mathScore;
    _labelReadingScore.text = _readingScore;
    _labelWritingScore.text = _writingScore;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
