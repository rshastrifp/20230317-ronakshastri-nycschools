//
//  SchoolDetailsViewController.h
//  NYCSchools
//
//  Created by Shastri, Ronak on 2023-03-16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SchoolDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelSchoolTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMathScore;
@property (weak, nonatomic) IBOutlet UILabel *labelReadingScore;
@property (weak, nonatomic) IBOutlet UILabel *labelWritingScore;

//Given more time I prefer moving these details in a struct or in view model.
//out of VC. 
@property (nonatomic, strong) NSString *schoolTitle;
@property (nonatomic, strong) NSString *mathScore;
@property (nonatomic, strong) NSString *readingScore;
@property (nonatomic, strong) NSString *writingScore;


@end

NS_ASSUME_NONNULL_END
