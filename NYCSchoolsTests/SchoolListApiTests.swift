//
//  SchoolListApiTests.swift
//  NYCSchoolsTests
//
//  Created by Shastri, Ronak on 2023-03-17.
//

import XCTest
@testable import NYCSchools

class MyAPITests: XCTestCase {
    
    //This is just for the demo purpose.
    //Given more time I would write few more code to change the environment and from static JSON pasrse the data.
    //Test the parsing code.
    //Also will add ability to change the config to test in different environment.
    //Also will add to test details fetch call. 
    func testFetchSchoolList() {
        let api = SchoolApi(apiQueries: SchoolApiQuery(limit: 50))
        let expectation = XCTestExpectation(description: "Data fetched successfully")
        
        api.fetchData { result in
            switch result {
            case .success(let list):
                XCTAssert(list.count == 50)
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Data fetch failed with error: \(error.localizedDescription)")
            }
        }
        
        wait(for: [expectation], timeout: 10.0) // Set timeout as per your requirement
    }
}
